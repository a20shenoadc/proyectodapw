-- MariaDB dump 10.19  Distrib 10.7.3-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: eventos
-- ------------------------------------------------------
-- Server version	10.7.3-MariaDB-1:10.7.3+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `evento_user`
--

DROP TABLE IF EXISTS `evento_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `evento_id` bigint(20) unsigned NOT NULL,
  `fecha_venta` date NOT NULL COMMENT 'Fecha de venta de la entrada',
  `fecha_evento` date NOT NULL COMMENT 'Fecha del evento para esta entrada',
  `precio` int(11) DEFAULT NULL COMMENT 'Precio de la entrada',
  `asiento` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Número de asiento',
  `codigoqrcontrol` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Codigo QR referente a la entrada.',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `evento_user_user_id_foreign` (`user_id`),
  KEY `evento_user_evento_id_foreign` (`evento_id`),
  CONSTRAINT `evento_user_evento_id_foreign` FOREIGN KEY (`evento_id`) REFERENCES `eventos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `evento_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento_user`
--

LOCK TABLES `evento_user` WRITE;
/*!40000 ALTER TABLE `evento_user` DISABLE KEYS */;
INSERT INTO `evento_user` VALUES
(1,4,3,'1970-03-19','1984-02-11',9586630,'63','5348669598939','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(2,1,8,'2009-08-06','2018-05-27',6647,'7','9806158877881','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(3,4,14,'1992-03-01','2014-08-26',2360601,'440','4605105972533','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(4,1,6,'2010-04-15','2019-08-06',5638,'68','4018936747285','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(5,5,15,'1975-08-30','1981-05-22',658,'740','5214618210026','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(6,1,13,'2014-03-31','1996-12-12',98,'6','0946250655112','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(7,5,15,'2013-01-12','1995-01-10',46507,'4','5924177638307','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(8,3,14,'2007-02-26','2005-05-07',844652733,'500','2665675218051','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(9,2,3,'1978-01-03','2013-12-24',347408611,'71','1412213884510','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(10,5,8,'1980-11-14','1979-01-12',64663,'501','2334354960092','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(11,2,14,'1997-09-29','1979-12-22',6,'570','9484144228949','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(12,5,10,'2007-10-15','2001-09-17',418718,'69','1939679658153','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(13,3,12,'1998-07-07','1970-12-13',199568,'13','0671262439756','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(14,5,9,'1998-10-23','1989-01-27',123638893,'7','4121759951484','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(15,2,3,'2019-12-11','1991-10-25',41645553,'89','5696433904654','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(16,2,15,'1988-07-26','2019-02-13',40047,'6','3960814958201','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(17,2,12,'1979-11-19','2010-03-03',8,'74','7863684729673','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(18,2,9,'1978-06-18','1976-08-07',17433185,'985','7906245366693','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(19,1,4,'1997-06-29','1970-12-06',30,'3','4313662726888','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(20,1,3,'2011-07-20','1990-11-22',6,'112','9941810677666','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(21,1,9,'2004-11-08','2017-10-02',70,'232','5433503108167','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(22,5,6,'1979-04-22','2007-03-23',289,'9','1359803656402','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(23,2,6,'1985-05-21','1998-04-27',6414182,'34','9549001060213','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(24,3,12,'1984-02-27','1992-12-14',540639581,'180','1214461014516','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(25,5,13,'2017-08-15','2007-08-29',30,'389','8888200591212','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(26,4,15,'2008-02-12','2018-09-27',15039,'1','2765580635995','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(27,2,9,'2002-10-15','1970-11-06',5325918,'4','0249347190900','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(28,2,10,'2015-03-23','2013-02-20',784,'230','3683204055204','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(29,3,9,'1990-10-16','1978-12-30',16,'62','8245106143523','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(30,5,3,'1977-01-13','2009-02-14',28501,'8','7138703019067','2022-03-22 13:58:39','2022-03-22 13:58:39');
/*!40000 ALTER TABLE `evento_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eventos`
--

DROP TABLE IF EXISTS `eventos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Titulo del evento',
  `telefono` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Teléfono del evento',
  `direccion` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Dirección del evento',
  `cartel` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Cartel/fotografía del evento.',
  `latitud` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Coordenadas geográficas: Latitud',
  `longitud` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Coordenadas geográficas: Longitud',
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventos`
--

LOCK TABLES `eventos` WRITE;
/*!40000 ALTER TABLE `eventos` DISABLE KEYS */;
INSERT INTO `eventos` VALUES
(1,'Espinal y Urbina SRL','+34 950-79-2583','Plaza Garza, 19, 3º C, 21507, La Tirado','cartel_por_defecto.jpg','69.100649','71.637214','2022-05-28','2022-05-29','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(2,'Blasco-Barajas','980 94 5322','Camino Rosa, 550, Entre suelo 3º, 15418, A Jaimes Baja','cartel_por_defecto.jpg','-64.423014','1.166104','2023-01-26','2023-02-23','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(3,'Viajes Montenegro-Pérez','+34 954-11-9910','Avinguda Delgadillo, 390, Ático 0º, 69753, Os Frías','cartel_por_defecto.jpg','-22.396879','56.722061','2022-07-22','2022-08-05','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(4,'Velázquez de Caraballo y Asoc.','983-884932','Ronda Aguayo, 5, 8º, 96216, Lugo Baja','cartel_por_defecto.jpg','33.027838','-93.70218','2023-10-02','2023-10-17','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(5,'Perea y Viera','+34 978-977859','Carrer Andrés, 5, Bajos, 27980, Las Toledo del Bages','cartel_por_defecto.jpg','62.759621','40.269761','2023-11-08','2023-11-23','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(6,'Viajes Rueda','957 00 1871','Avenida Lara, 85, 85º F, 39130, Villalba del Puerto','cartel_por_defecto.jpg','84.727651','38.332737','2024-02-21','2024-02-22','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(7,'Air Murillo','+34 939-574700','Carrer Miramontes, 28, 25º A, 60609, O Tejeda de San Pedro','cartel_por_defecto.jpg','19.656501','-74.167523','2022-10-02','2022-10-24','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(8,'Gestora Quintana','970 834125','Camiño Claudia, 77, Bajo 1º, 72880, Benítez de San Pedro','cartel_por_defecto.jpg','-32.975599','-120.12159','2023-01-07','2023-01-30','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(9,'Sánchez de Barreto S. de H.','925 514202','Calle Zarate, 66, Entre suelo 1º, 48288, Los Villa de las Torres','cartel_por_defecto.jpg','-52.773348','157.420706','2022-12-17','2023-01-04','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(10,'Esteban, Trujillo y Leiva SA','909 182644','Carrer Margarita, 578, 96º A, 17143, Las Mondragón','cartel_por_defecto.jpg','50.750445','-7.101122','2022-06-22','2022-06-29','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(11,'Carrillo de Quiñones e Hijos','+34 919-98-5397','Plaça Gimeno, 237, 2º C, 97881, O Soto de Lemos','cartel_por_defecto.jpg','-13.494996','52.053267','2023-07-20','2023-08-11','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(12,'Ureña de Candelaria SA','934 42 5528','Travessera Ángeles, 70, 5º E, 82943, Mercado Alta','cartel_por_defecto.jpg','-53.28353','-85.071232','2023-05-18','2023-06-17','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(13,'Loera de Puig','907058433','Avenida Dávila, 804, 19º E, 70763, Los Cabello de Arriba','cartel_por_defecto.jpg','61.086866','177.478381','2024-01-31','2024-02-05','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(14,'Empresa Dávila-Alarcón','957 23 4287','Avinguda Yolanda, 230, 63º F, 85465, Los Navarro de la Sierra','cartel_por_defecto.jpg','-20.562136','8.60158','2023-06-13','2023-07-11','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(15,'Bahena y Bahena e Hijo','+34 979-35-8339','Camino Lira, 63, 86º D, 78481, Domenech de las Torres','cartel_por_defecto.jpg','41.977878','172.218173','2023-03-06','2023-03-09','2022-03-22 13:58:39','2022-03-22 13:58:39');
/*!40000 ALTER TABLE `eventos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES
(17,'2014_10_12_000000_create_users_table',1),
(18,'2014_10_12_100000_create_password_resets_table',1),
(19,'2014_10_12_200000_add_two_factor_columns_to_users_table',1),
(20,'2019_08_19_000000_create_failed_jobs_table',1),
(21,'2019_12_14_000001_create_personal_access_tokens_table',1),
(22,'2022_01_17_095544_create_sessions_table',1),
(23,'2022_01_17_102736_create_eventos_table',1),
(24,'2022_01_17_104834_create_evento_user_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sessions_user_id_index` (`user_id`),
  KEY `sessions_last_activity_index` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES
('KhgBfp6ZaTeorPzi6q5vffkbOaZbPD3NaThb3YAz',NULL,'10.0.2.15','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:98.0) Gecko/20100101 Firefox/98.0','YTozOntzOjY6Il90b2tlbiI7czo0MDoiUFI0UUw2TDJOZFJQeHB3YlQyWFBHVHBaWUtMMVdBNmhGblZxbXpPbCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjc6Imh0dHA6Ly90ZXN0LmRhcHcubXl3aXJlLm9yZyI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=',1647958589);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) unsigned DEFAULT NULL,
  `profile_photo_path` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES
(1,'Izan Lucas Tercero','Esparza','wquiroz@example.org','2022-03-22 13:58:39','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,'XX9n0jFfQ9',NULL,NULL,'+34 970-40-2106','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(2,'Hugo Moreno','Correa','barraza.mariapilar@example.net','2022-03-22 13:58:39','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,'Scnmn7zde5',NULL,NULL,'+34 927-079102','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(3,'Dr. Esther Espinal','Brito','gsalinas@example.com','2022-03-22 13:58:39','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,'ZYO5ye7rag',NULL,NULL,'940574007','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(4,'Alexia Torres','Toro','zmerino@example.net','2022-03-22 13:58:39','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,'pm96AGVSjW',NULL,NULL,'938 28 0929','2022-03-22 13:58:39','2022-03-22 13:58:39'),
(5,'Alex Valencia','Soto','rosales.salma@example.net','2022-03-22 13:58:39','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,'lDLU4zjb9r',NULL,NULL,'961258758','2022-03-22 13:58:39','2022-03-22 13:58:39');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-23 16:11:23
