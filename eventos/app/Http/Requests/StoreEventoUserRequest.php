<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEventoUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'evento_id' => 'exists:eventos,id',
            'fecha_evento' => 'date|required',
            'precio' => 'nullable|max:11',
            'asiento' => 'required|max:15',
        ];
    }
}
