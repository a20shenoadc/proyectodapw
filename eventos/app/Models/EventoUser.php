<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EventoUser extends Pivot
{
    use HasFactory;

    protected $fillable = array('user_id', 'evento_id', 'fecha_venta', 'fecha_evento', 'precio', 'asiento', 'codigoqrcontrol');
}
