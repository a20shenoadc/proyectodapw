@extends('plantillas.master')

@section('title')
Información sobre Eventos
@stop

@section('central')
<h2>Información de Evento</h2>
<hr />
Titulo: {{ $evento->titulo }}<br />
Teléfono: {{ $evento->telefono }}<br />
Dirección: {{ $evento->direccion }}<br />
Fotografía del cartel: <img src='{{ URL::asset("storage/$evento->cartel") }}' width="320" /><br />
<hr />
Situación:
<iframe width="600" height="600" src="https://maps.google.com/maps?q={{ $evento->latitud }},{{ $evento->longitud }}&output=embed">
</iframe>
<br />
Fecha Inicio Evento: {{ \Carbon\Carbon::parse($evento->fecha_inicio)->format('d/m/Y') }}<br />
Fecha Fin Evento: {{ \Carbon\Carbon::parse($evento->fecha_fin)->format('d/m/Y') }}<br />
@stop